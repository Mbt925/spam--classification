from sklearn.feature_extraction.text import CountVectorizer
from sklearn.feature_extraction.text import TfidfTransformer

from tools.data_processor import *
from tools.file_manager import *


def tf_idf_features(X_train, X_val, X_test, size=2000):
    vectorizer = TfidfTransformer(smooth_idf=True, use_idf=True)
    counter = CountVectorizer(max_df=0.85, max_features=size)  # ignore words which appear in 85% of documents

    word_count_vector = counter.fit_transform(X_train)
    vectorizer.fit(word_count_vector)

    # train
    count_vector = counter.transform(X_train)
    X_train_features = vectorizer.transform(count_vector)

    # validation
    X_val_features = []
    if X_val:
        count_vector = counter.transform(X_val)
        X_val_features = vectorizer.transform(count_vector)

    # test
    count_vector = counter.transform(X_test)
    X_test_features = vectorizer.transform(count_vector)

    return X_train_features, X_val_features, X_test_features


def extract_tf_idf(file_path, val_ratio=0.15, test_ratio=0.2, size=2000):
    data = load_json(file_path)
    X, y = convert_to_xy(data)

    X_train, X_val, X_test, y_train, y_val, y_test = split_train_val_test(X, y, val_ratio, test_ratio)

    X_train_features, X_val_features, X_test_features = tf_idf_features(X_train, X_val, X_test, size=size)

    return X_train_features, X_val_features, X_test_features, y_train, y_val, y_test
