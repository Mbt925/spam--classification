import numpy as np
from keras.preprocessing.sequence import pad_sequences
from keras.preprocessing.text import Tokenizer

from tools.data_processor import *
from tools.file_manager import *


def keras_bow_features(X_train, X_val, X_test, max_feature=5000, max_len=2000):
    tokenizer = Tokenizer(num_words=max_feature)
    tokenizer.fit_on_texts(X_train)

    X_train_features = np.array(tokenizer.texts_to_sequences(X_train))
    X_val_features = np.array(tokenizer.texts_to_sequences(X_val))
    X_test_features = np.array(tokenizer.texts_to_sequences(X_test))

    X_train_features = pad_sequences(X_train_features, maxlen=max_len)
    X_val_features = pad_sequences(X_val_features, maxlen=max_len)
    X_test_features = pad_sequences(X_test_features, maxlen=max_len)

    return X_train_features, X_val_features, X_test_features


def extract_keras_bow(file_path, val_ratio=0.15, test_ratio=0.2, max_feature=5000, max_len=2000):
    data = load_json(file_path)
    X, y = convert_to_xy(data)

    X_train, X_val, X_test, y_train, y_val, y_test = split_train_val_test(X, y, val_ratio, test_ratio)

    X_train_features, X_val_features, X_test_features = keras_bow_features(X_train, X_val, X_test, max_feature,
                                                                           max_len)
    return X_train_features, X_val_features, X_test_features, y_train, y_val, y_test
