import numpy as np
from sklearn.feature_extraction.text import CountVectorizer
from sklearn.feature_extraction.text import TfidfTransformer
import gensim

from tools.data_processor import *
from tools.file_manager import *


class tfIdfEmbeddingVectorizer(object):
    def __init__(self, word2vec):
        self.word2vec = word2vec
        self.word2weight = None
        self.dim = len(word2vec[list(word2vec.keys())[0]])

    def fit(self, X):
        vectorizer = TfidfTransformer(smooth_idf=True, use_idf=True)
        counter = CountVectorizer()

        word_count_vector = counter.fit_transform(X)
        vectorizer.fit(word_count_vector)

        self.word2weight = dict(zip(counter.get_feature_names(), vectorizer.idf_))
        return self

    def transform(self, X):
        return np.array([
            np.mean([self.word2vec[w] * self.word2weight[w]
                     for w in words if w in self.word2vec] or
                    [np.zeros(self.dim)], axis=0)
            for words in X
        ])


# convert each row of X to list of words
def tokenize_data(data):
    data_tokenized = []
    for row in data:
        data_tokenized.append(row.split())
    return data_tokenized


def tf_idf_embedding_features(X_train, X_val, X_test, size=2000):
    X_train_tokenized = tokenize_data(X_train)
    X_val_tokenized = tokenize_data(X_val) if X_val else []
    X_test_tokenized = tokenize_data(X_test)

    model = gensim.models.Word2Vec(X_train_tokenized, size=size)
    w2v = dict(zip(model.wv.index2word, model.wv.syn0))

    vectorizer = tfIdfEmbeddingVectorizer(w2v)
    vectorizer.fit(X_train)

    # train
    X_train_features = vectorizer.transform(X_train_tokenized)

    # validation
    X_val_features = vectorizer.transform(X_val_tokenized)

    # test
    X_test_features = vectorizer.transform(X_test_tokenized)

    return X_train_features, X_val_features, X_test_features


def extract_tf_idf_embedding(file_path, val_ratio=0.15, test_ratio=0.2, size=2000):
    data = load_json(file_path)
    X, y = convert_to_xy(data)

    X_train, X_val, X_test, y_train, y_val, y_test = split_train_val_test(X, y, val_ratio, test_ratio)

    X_train_features, X_val_features, X_test_features = tf_idf_embedding_features(X_train, X_val, X_test, size=size)

    return X_train_features, X_val_features, X_test_features, y_train, y_val, y_test
