from tools.file_manager import *
import re
from bs4 import BeautifulSoup
import warnings
import string

from nltk.tokenize import word_tokenize
from nltk.stem import WordNetLemmatizer
from nltk.corpus import stopwords
import nltk


def trim_html_tags(raw_dict):
    warnings.filterwarnings("ignore", category=UserWarning, module='bs4')

    for row in raw_dict:
        row['body'] = BeautifulSoup(row['body'], features="lxml").get_text()
    return raw_dict


def remove_ws_punc_nums(raw_dict):
    table = str.maketrans('', '', string.punctuation)
    for row in raw_dict:
        row['body'] = row['body'].replace('\n', ' ').replace('\t', ' ').strip().lower()
        row['body'] = row['body'].translate(table)
        row['body'] = re.sub(' +', ' ', row['body'])
        row['body'] = re.sub(r'\d+', '', row['body'])
    return raw_dict


def is_ascii(str):
    try:
        str.encode('ascii')
    except UnicodeEncodeError:
        return False
    else:
        return True


def remove_non_latin(raw_dict):
    raw_dict = [row for row in raw_dict if is_ascii(row['body'])]
    return raw_dict


def remove_self_sent(raw_dict):
    new_dict = []
    for i in range(len(raw_dict)):
        row = raw_dict[i]
        from_split = re.findall(r'[\w\.-]+@[\w\.-]+', row['from']) if row['from'] is not None else []
        to_split = re.findall(r'[\w\.-]+@[\w\.-]+', row['to']) if row['to'] is not None else []
        cc_split = re.findall(r'[\w\.-]+@[\w\.-]+', row['cc']) if row['cc'] is not None else []
        bcc_split = re.findall(r'[\w\.-]+@[\w\.-]+', row['bcc']) if row['bcc'] is not None else []

        if not list(set(from_split) & set(to_split)) and \
                not list(set(from_split) & set(cc_split)) and \
                not list(set(from_split) & set(bcc_split)):
            row['from'] = ' '.join(str(x) + ',' for x in from_split)
            row['to'] = ' '.join(str(x) + ',' for x in to_split)
            row['cc'] = ' '.join(str(x) + ',' for x in cc_split)
            row['bcc'] = ' '.join(str(x) + ',' for x in bcc_split)
            new_dict.append(row)
    return new_dict


def remove_empty_body(raw_dict):
    raw_dict = [row for row in raw_dict if row['body'].strip() and not row['body'] is None]
    return raw_dict


def remove_stopwords_lem_stem(raw_dict):
    # nltk.download()
    stop_words = set(stopwords.words('english'))
    ps = nltk.PorterStemmer()
    wnl = WordNetLemmatizer()

    for row in raw_dict:
        words = word_tokenize(row['body'])
        filtered_words = []
        for word in words:
            if word not in stop_words:
                word = wnl.lemmatize(word)
                word = ps.stem(word)
                if len(word) > 1:
                    filtered_words.append(word)
        row['body'] = ' '.join(filtered_words)
    return raw_dict


def just_keep_body(raw_dict):
    data = []
    for row in raw_dict:
        data.append({'class': row['class'], 'body': row['body']})
    return data


# remove emails with empty or non-latin bodies, sent to the sender, strip html tags and stopwords, stemming, lemmatization
def clean_all(file_path):
    raw_dict = load_json(file_path)

    # report the number of spams & hams
    print('Before cleaning --------- Spams: {}, Hams: {}'.format(sum(1 for row in raw_dict if row['class'] == 'spam'),
                                                                 sum(1 for row in raw_dict if row['class'] == 'ham')))
    raw_dict = remove_self_sent(raw_dict)
    print('Removing self sent emails: done')
    raw_dict = remove_empty_body(raw_dict)
    print('Removing emails with empty body: done')
    raw_dict = trim_html_tags(raw_dict)
    print('Trimming html tags: done')
    raw_dict = remove_non_latin(raw_dict)
    print('Removing non-latin emails: done')
    raw_dict = remove_ws_punc_nums(raw_dict)
    print('Removing whitespaces, punctuations, and numbers: done')
    raw_dict = remove_stopwords_lem_stem(raw_dict)
    print('Removing stopwords, applying stemming and lemmatization: done')
    raw_dict = just_keep_body(raw_dict)
    print('Removing other parts except body and class: done')
    raw_dict = remove_empty_body(raw_dict)
    print('Removing emails with empty bodies: done')

    # report the number of spams & hams
    print('After cleaning --------- Spams: {}, Hams: {}'.format(sum(1 for row in raw_dict if row['class'] == 'spam'),
                                                                sum(1 for row in raw_dict if row['class'] == 'ham')))
    save_json('data/preprocessed.json', raw_dict)
