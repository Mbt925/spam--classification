import email
import os

from tools.file_manager import *


def get_message_body(message):
    body = ''  # no body
    if message.is_multipart():
        for part in message.walk():
            ctype = part.get_content_type()
            cdispo = str(part.get('Content-Disposition'))

            # skip any text/plain attachments
            if (ctype == 'text/plain' or ctype == 'text/html') and 'attachment' not in cdispo:
                body = part.get_payload()  # decode
                break
    # not multipart - i.e. plain text, no attachments
    else:
        body = message.get_payload()
    return body


def read_all_files(parent_folder_path, is_ham=True):
    raw_dict = []
    for file_name in os.listdir(parent_folder_path):
        full_path = os.path.join(parent_folder_path, file_name)
        if os.path.isdir(full_path):
            raw_dict.extend(read_all_files(full_path, is_ham))
        else:
            file = open(full_path, "r")
            content = file.read()
            email_dict = email.message_from_string(content)
            raw_dict.append(
                {'class': 'ham' if is_ham else 'spam', 'to': email_dict['To'], 'cc': email_dict['Cc'],
                 'bcc': email_dict['Bcc'],
                 'from': email_dict['From'], 'subject': email_dict['Subject'],
                 'date': email_dict['Date'], 'body': get_message_body(email_dict)})

    return raw_dict


# convert the files to a single well-formed json file
def fix_all(corpus_folder_path):
    raw_dict = read_all_files(os.path.join(corpus_folder_path, 'ham'), True)
    raw_dict.extend(read_all_files(os.path.join(corpus_folder_path, 'spam'), False))

    save_json('data/raw.json', raw_dict)
