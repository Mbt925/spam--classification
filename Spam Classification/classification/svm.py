from feature_extraction.tf_idf import extract_tf_idf
from feature_extraction.keras_bow import extract_keras_bow
from feature_extraction.tf_idf_embedding import extract_tf_idf_embedding
from sklearn import svm
from sklearn import metrics


def train_svm():
    X_train_features, _, X_test_features, y_train, _, y_test = extract_tf_idf_embedding('data/preprocessed.json', val_ratio=0.0,
                                                                              test_ratio=0.3, size=2000)
    clf = svm.LinearSVC()
    clf.fit(X_train_features, y_train)

    y_pred = clf.predict(X_test_features)

    print("SVM evaluation is done.")
    print("Accuracy:", metrics.accuracy_score(y_test, y_pred))
    print("Precision:", metrics.precision_score(y_test, y_pred))
    print("Recall:", metrics.recall_score(y_test, y_pred))
    print("F1-Measure:", metrics.f1_score(y_test, y_pred))

# tf_idf, test ratio: 0.3, size = all
# Accuracy: 0.9865644724977457
# Precision: 0.9809568044588945
# Recall: 0.9959132348318139
# F1-Measure: 0.9883784416192186

# tf_idf, test ratio: 0.3, size = 5000
# Accuracy: 0.9827772768259694
# Precision: 0.9781496978149697
# Recall: 0.9921408362150267
# F1-Measure: 0.9850955911041747

# tf_idf, test ratio: 0.3, size = 2000
# Accuracy: 0.9785392245266006
# Precision: 0.9745815251084935
# Recall: 0.9883684375982396
# F1-Measure: 0.981426564694865

# embedding tf_idf, test ratio: 0.3, size = 5000
# Accuracy: 0.975653742110009
# Precision: 0.9760862769615505
# Recall: 0.9816095567431625
# F1-Measure: 0.9788401253918496

# embedding tf_idf, test ratio: 0.3, size = 2000
# Accuracy: 0.9750225428313796
# Precision: 0.9781549583529782
# Recall: 0.9783087079534738
# F1-Measure: 0.9782318271119842
