import tensorflow
from keras.layers import CuDNNGRU
from keras.layers import Bidirectional, GlobalMaxPool1D
from keras.layers import Dense, Input, Embedding, Dropout
from keras.models import Model
from sklearn import metrics
import matplotlib.pyplot as plt

from feature_extraction.keras_bow import extract_keras_bow
from feature_extraction.tf_idf import extract_tf_idf
from feature_extraction.tf_idf_embedding import extract_tf_idf_embedding


def train_rnn():
    embed_size = 100  # each word embedding vector size
    max_feature = 5000  # number of unique words to use (num of rows in embedding vector)
    max_len = 2000  # max number of words to use

    X_train_features, X_val_features, X_test_features, y_train, y_val, y_test = extract_keras_bow(
        'data/preprocessed.json', max_feature=max_feature, max_len=max_len)

    # load json and create model
    # model = keras.models.load_model('rnn_uw5000_em100_w2000_ep5.h5')

    # with tf.device("/cpu:0"):
    inp = Input(shape=(max_len,))
    x = Embedding(max_feature, embed_size)(inp)
    x = Bidirectional(CuDNNGRU(64, return_sequences=True))(x)
    x = GlobalMaxPool1D()(x)
    x = Dense(16, activation="relu")(x)
    x = Dropout(0.1)(x)
    x = Dense(1, activation="sigmoid")(x)
    model = Model(inputs=inp, outputs=x)
    model.compile(loss='binary_crossentropy', optimizer='adam', metrics=['accuracy'])
    print(model.summary())

    history = model.fit(X_train_features, y_train, batch_size=60, epochs=5, validation_data=(X_val_features, y_val))

    # save the model
    model.save('rnn_uw5000_em100_w2000_ep5.h5')

    # display learning curve
    print(history.history.keys())
    plt.plot(history.history['accuracy'])
    plt.plot(history.history['val_accuracy'])
    plt.title('model accuracy')
    plt.ylabel('accuracy')
    plt.xlabel('epoch')
    plt.legend(['train', 'validation'], loc='upper left')
    plt.show()

    # calculate prediction of the model on the test set
    y_pred = [1 if o > 0.5 else 0 for o in model.predict(X_test_features)]
    # confusion_matrix(y_test, y_pred)

    print("RNN evaluation is done.")
    print("Accuracy:", metrics.accuracy_score(y_test, y_pred))
    print("Precision:", metrics.precision_score(y_test, y_pred))
    print("Recall:", metrics.recall_score(y_test, y_pred))
    print("F1-Measure:", metrics.f1_score(y_test, y_pred))

# rnn_unw5000_em100_w20000_ep15
# Accuracy: 0.9864778534923339
# Precision: 0.9875786417164059
# Recall: 0.991898898250162
# F1-Measure: 0.9897340554522673
