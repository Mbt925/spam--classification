from sklearn.model_selection import train_test_split


def convert_to_xy(data):
    X = [row['body'] for row in data]
    y = [0 if row['class'] == 'ham' else 1 for row in data]

    return X, y


def split_train_val_test(X, y, val_ratio, test_ratio):
    X_train, X_test, y_train, y_test = train_test_split(X, y, test_size=test_ratio, random_state=925)
    X_val = []
    y_val = []

    if val_ratio > 0.0:
        X_train, X_val, y_train, y_val = train_test_split(X_train, y_train, test_size=val_ratio * (1 - test_ratio),
                                                          random_state=926)

    # report split ratio and the number of samples
    print('Split train test with {} validation ratio and {} test ratio'.format(val_ratio, test_ratio))
    print('Overall number of data samples: {}'.format(len(X)))
    print('Number of training samples: {}'.format(len(X_train)))
    print('Number of validation samples: {}'.format(len(X_val)))
    print('Number of test samples: {}'.format(len(X_test)))

    train_spams_count = sum([row for row in y_train])
    val_spams_count = sum([row for row in y_val])
    test_spams_count = sum([row for row in y_test])

    print('Spam ratio in training data: {}'.format(train_spams_count / len(X_train)))
    print('Spam ratio in validation data: {}'.format(val_spams_count / len(X_val) if len(X_val) > 0 else 0))
    print('Spam ratio in test data: {}'.format(test_spams_count / len(X_test)))

    return X_train, X_val, X_test, y_train, y_val, y_test
