import json


def save_json(file_path, _dict):
    with open(file_path, 'w', encoding="utf8") as fp:
        fp.write(json.dumps(_dict, indent=4))


def load_json(file_path):
    with open(file_path, 'r') as fp:
        return json.load(fp)
